const inputKod = document.querySelector('.inputKod');
const inputDate = document.querySelector('.inputDate');
const inputNomer = document.querySelector('.inputNomer');
const inputUsername = document.querySelector('.inputUsername');
const inputHudud = document.querySelector('.inputHudud');

const formEl = document.querySelector('form');
const tbodyEl = document.querySelector('tbody');
const tableEl = document.querySelector('table');

function onAddWebsite(e){
    e.preventDefault();
    // alert(inputKod.value+inputUsername.value);

    tbodyEl.innerHTML += `
    <tr>
        <td>${inputKod.value}<td>
        <td>${inputDate.value}<td>
        <td>${inputNomer.value}<td>
        <td>${inputUsername.value}<td>
        <td>${inputHudud.value}<td>
        <td><button class="deleteBtn btn btn-success">Delete<button></td>
    `
}

function onDeleteRow(e) {
    if (!e.target.classList.contains("deleteBtn")) {
      return;
    }

    const btn = e.target;
    btn.closest("tr").remove();
  }
  
formEl.addEventListener('submit',onAddWebsite);
tableEl.addEventListener('click',onDeleteRow);
